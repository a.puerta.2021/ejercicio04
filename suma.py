import math
import unittest
from fraccion import Fraccion

class TestSuma(unittest.TestCase):
    def suma(self, f2):
        if self.denominador == f2.denominador:
            self.numerador = self.numerador + f2.numerador
            self.denominador = f2.denominador
        else:
            denominador_comun= math.lcm(self, f2)
            self.numerador = (denominador_comun/self.denominador)*self.numerador + (denominador_comun/f2.denominador)*f2.numerador