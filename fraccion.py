import math

def lcm(a,b):
  return (a * b) // math.gcd(a,b)

class Fraccion:
    def __init__(self, num, den):
        self.numerador = num
        self.denominador = den
    
    def __str__(self):
        return str(self.numerador) + "/" + str(self.denominador)

    def suma(self, f2):        
        if self.denominador == f2.denominador:
            self.numerador = self.numerador + f2.numerador
            self.denominador = f2.denominador
        else:
            denominador_comun = lcm(self.denominador, f2.denominador)
            self.numerador = (denominador_comun/self.denominador)*self.numerador + (denominador_comun/f2.denominador)*f2.numerador
            self.denominador = denominador_comun
        return self


    def resta(self, f2):
        if self.denominador == f2.denominador:
            self.numerador = self.numerador - f2.numerador
            self.denominador = f2.denominador   
        else:
            denominador_comun= lcm(self.denominador, f2.denominador)
            self.numerador = (denominador_comun/self.denominador)*self.numerador - (denominador_comun/f2.denominador)*f2.numerador
            self.denominador = denominador_comun
        return self

    def multiplica(self, f2):
        self.numerador = self.numerador * f2.numerador
        self.denominador = self.denominador * f2.denominador
        return self

    def divide(self, f2):
        self.numerador = self.numerador * f2.denominador
        self.denominador = self.denominador * f2.numerador
        return self
    

    
