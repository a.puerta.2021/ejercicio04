import math
class Fraccion:
    def __init__(self, num, den):
        self.numerador = num
        self.denominador = den
    
    def __str__(self):
        return str(self.numerador) + "/" + str(self.denominador)
    def suma(self, f2):
        if self.denominador == f2.denominador:
            self.numerador = self.numerador + f2.numerador
            self.denominador = f2.denominador
        else:
            denominador_comun= math.lcm(self, f2)
            self.numerador = (denominador_comun/self.denominador)*self.numerador + (denominador_comun/f2.denominador)*f2.numerador


    def resta(self, f2):
        if self.denominador == f2.denominador:
            self.numerador = self.numerador - f2.numerador
            self.denominador = f2.denominador   
        else:
            denominador_comun= math.lcm(self, f2)
            self.numerador = (denominador_comun/self.denominador)*self.numerador - (denominador_comun/f2.denominador)*f2.numerador


    def multiplica(self, f2):
        self.numerador = self.numerador * f2.numerador
        self.denominador = self.denominador * f2.denominador

    def divide(self, f2):
        self.numerador = self.numerador * f2.denominador
        self.denominador = self.denominador * f2.numerador
    
    
