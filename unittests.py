import unittest
from fraccion import Fraccion

class TestOperaciones(unittest.TestCase):
    def test_suma(self):
        
        f1 = Fraccion(1,2)
        f2 = Fraccion(3,8)

        suma = f1.suma(f2)

        self.assertEqual(suma.numerador, 7)
        self.assertEqual(suma.denominador, 8)

    def test_resta(self):
        
        f1 = Fraccion(5,3)
        f2 = Fraccion(1,3)

        resta = f1.resta(f2)

        self.assertEqual(resta.numerador, 4)
        self.assertEqual(resta.denominador, 3)

    def test_multiplica(self):
        
        f1 = Fraccion(3,8)
        f2 = Fraccion(9,5)

        multiplica = f1.multiplica(f2)

        self.assertEqual(multiplica.numerador, 27)
        self.assertEqual(multiplica.denominador, 40)

    def test_divide(self):
        
        f1 = Fraccion(1,2)
        f2 = Fraccion(3,4)

        divide = f1.divide(f2)

        self.assertEqual(divide.numerador, 4)
        self.assertEqual(divide.denominador, 6)
        
if __name__ == "__main__":
    unittest.main()